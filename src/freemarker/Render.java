package freemarker;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class Render {
    public static void rend(
            HttpServletRequest request, PrintWriter responseWriter, String templateName, Map<String, Object> data
    ) {
        Configuration cfg = FreeMarkerConfig.getConfig(request);
        try {
            Template tmpl = cfg.getTemplate(templateName);
            tmpl.process(data, responseWriter);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }
}

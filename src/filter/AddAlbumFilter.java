package filter;

import model.Pages;
import model.Parameters;
import model.User;
import service.SessionService;
import service.UserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddAlbumFilter extends HttpFilter {
    private SessionService sessionService = SessionService.get();
    private UserService userService = UserService.get();

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        User user = userService.getUserById(sessionService.getCurrentUserId(req));
        if (user == null || !user.isPerformer()) {
            resp.sendRedirect(Pages.PROFILE);
        } else if ("post".equals(req.getMethod())) {
            if (checkParams(req, Parameters.TITLE, Parameters.GENRE, Parameters.YEAR)) {
                chain.doFilter(req, resp);
            } else resp.sendRedirect(Pages.ADD_ALBUM);
        } else chain.doFilter(req, resp);
    }
}

package filter;

import model.Pages;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static model.Parameters.*;

public class RegistrationFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        boolean isPost = "post".equals(req.getMethod());
        if (isPost) {
            if (checkParams(req, USERNAME, EMAIL, PASSWORD, CONFIRM, COUNTRY, IS_PERFORMER)
                    && req.getParameter(PASSWORD).equals(req.getParameter(CONFIRM))) {
                chain.doFilter(req, resp);
            } else resp.sendRedirect(Pages.REGISTRATION);
        } else chain.doFilter(req, resp);
    }
}

package filter;

import model.Pages;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static model.Parameters.*;

public class EditProfileFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        boolean isPost = "post".equals(req.getMethod());
        if (!isPost || checkParams(req, USERNAME, EMAIL, COUNTRY, CAN_RECEIVE_MSG)) {
            chain.doFilter(req, resp);
        } else resp.sendRedirect(Pages.EDIT_PROFILE);
    }
}

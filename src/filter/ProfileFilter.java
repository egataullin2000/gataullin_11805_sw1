package filter;

import model.Pages;
import model.Parameters;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ProfileFilter extends HttpFilter {
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        if (checkParams(req, Parameters.ID) || !needAuth(req)) {
            chain.doFilter(req, resp);
        } else resp.sendRedirect(Pages.LOGIN);
    }
}

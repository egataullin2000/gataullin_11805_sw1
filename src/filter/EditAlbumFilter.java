package filter;

import model.Album;
import model.Pages;
import service.AlbumService;
import service.SessionService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static model.Parameters.*;

public class EditAlbumFilter extends HttpFilter {
    private SessionService sessionService = SessionService.get();
    private AlbumService albumService = AlbumService.get();

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        if (!canEdit(req)) {
            resp.sendRedirect(Pages.ALBUM + "?id=" + req.getParameter(ID));
        } else if (!"post".equals(req.getMethod())) {
            chain.doFilter(req, resp);
        } else {
            boolean hasNecessaryParams = checkParams(req, ID, TITLE, GENRE, YEAR);
            if (hasNecessaryParams && canEdit(req)) {
                chain.doFilter(req, resp);
            } else resp.sendRedirect(Pages.ALBUM + "?" + ID + "=" + req.getParameter(ID));
        }
    }

    private boolean canEdit(HttpServletRequest request) {
        int albumId = Integer.parseInt(request.getParameter(ID));
        Album album = albumService.getAlbumById(albumId);

        return album.getPerformerId() == sessionService.getCurrentUserId(request);
    }
}

package dao;

import model.Album;
import model.Audio;
import model.Comment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AlbumsDao extends AbstractDao<Album> {
    private static final String TABLE = "albums";

    private AudiosDao audiosDao = new AudiosDao();
    private CommentsDao commentsDao = new CommentsDao();

    public AlbumsDao() {
        super(TABLE);
    }

    @Override
    protected Album parseEntityFrom(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt(1);
        Album album = new Album(
                id,
                resultSet.getInt(2),
                resultSet.getString(3),
                resultSet.getString(4),
                resultSet.getString(5),
                resultSet.getString(6),
                resultSet.getInt(7),
                resultSet.getString(8)
        );
        album.setAudios(getAudios(id));
        album.setComments(getComments(id));
        return album;
    }

    private List<Audio> getAudios(int albumId) {
        return audiosDao.getEntitiesWith(
                new String[]{Audio.FIELDS[1]},
                new String[]{Integer.toString(albumId)}
        );
    }

    private List<Comment> getComments(int albumId) {
        return commentsDao.getEntitiesWith(
                new String[]{Comment.FIELDS[1]},
                new String[]{Integer.toString(albumId)}
        );
    }

    public List<Album> search(String query) {
        String sql = getSqlBuilder()
                .select(Columns.ALL)
                .from("music_base_db." + TABLE)
                .where("title LIKE ?")
                .build();

        List<Album> result = new ArrayList<>();
        try (PreparedStatement statement = getPreparedStatement(sql)) {
            statement.setString(1, "%" + query + "%");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(parseEntityFrom(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }
}

package dao;

import model.Album;
import model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UsersDao extends AbstractDao<User> {
    private static final String TABLE = "users";

    public UsersDao() {
        super(TABLE);
    }

    @Override
    protected User parseEntityFrom(ResultSet resultSet) throws SQLException {
        User user = new User(
                resultSet.getInt(1),
                resultSet.getBoolean(2),
                resultSet.getString(3),
                resultSet.getString(4),
                resultSet.getString(5),
                resultSet.getBoolean(6)
        );
        user.setPhotoSrc(resultSet.getString(7));
        user.setDescription(resultSet.getString(8));
        if (user.isPerformer()) {
            List<Album> albums = new AlbumsDao().getEntitiesWith(
                    new String[]{Album.FIELDS[1]},
                    new String[]{Integer.toString(user.getId())}
            );
            user.setAlbums(albums);
        }

        return user;
    }
}

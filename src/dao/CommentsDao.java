package dao;

import model.Comment;
import model.User;
import service.UserService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

public class CommentsDao extends AbstractDao<Comment> {
    private static final String TABLE = "comments";

    private UserService userService = UserService.get();

    public CommentsDao() {
        super(TABLE);
    }

    @Override
    protected Comment parseEntityFrom(ResultSet resultSet) throws SQLException {
        User commentator = userService.getUserById(resultSet.getInt(3));

        Calendar date = Calendar.getInstance();
        date.setTime(resultSet.getDate(5));

        return new Comment(
                resultSet.getInt(1),
                resultSet.getInt(2),
                commentator,
                resultSet.getString(4),
                date
        );
    }
}

package dao;

import model.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDao<E extends Entity> {
    private Connection connection;
    private String table;

    AbstractDao(String table) {
        connection = ConnectionPool.getConnection();
        this.table = "music_base_db." + table;
    }

    protected abstract E parseEntityFrom(ResultSet resultSet) throws SQLException;

    public boolean insert(E entity) {
        boolean modified = false;

        try {
            String sql = getSqlBuilder()
                    .insertInto(table)
                    .values(entity.getValues())
                    .build();

            PreparedStatement statement = getPreparedStatement(sql);
            String[] values = entity.getValues();
            for (int i = 0; i < values.length; i++) {
                statement.setNString(i + 1, values[i]);
            }

            modified = statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return modified;
    }

    public E getEntityById(int id) {
        String sql = getSqlBuilder()
                .select(Columns.ALL)
                .from(table)
                .where(Columns.ID + " = ?")
                .build();

        List<E> result = executeQuery(sql, new String[]{Integer.toString(id)});
        return result.isEmpty() ? null : result.get(0);
    }

    public E getEntityWith(String[] columns, String[] values) {
        List<E> result = getEntitiesWith(columns, values);
        return result.isEmpty() ? null : result.get(0);
    }

    public List<E> getEntitiesWith(String[] columns, String[] values) {
        String sql = getSqlBuilder()
                .select(Columns.ALL)
                .from(table)
                .where(getWhereClauseFor(columns))
                .build();

        return executeQuery(sql, values);
    }

    public List<E> getEntities(String whereClause, String... whereArgs) {
        String sql = getSqlBuilder()
                .select(Columns.ALL)
                .where(whereClause)
                .build();

        return executeQuery(sql, whereArgs);
    }

    public List<E> getAll() {
        String sql = getSqlBuilder()
                .select(Columns.ALL)
                .from(table)
                .build();

        return executeQuery(sql, new String[0]);
    }

    public int update(E entity) {
        String sql = getSqlBuilder()
                .update(table)
                .set(entity.getFields())
                .where(Columns.ID + " = ?")
                .build();

        String[] entityValues = entity.getValues();
        String[] values = new String[entityValues.length + 1];
        for (int i = 0; i < entity.getValues().length; i++) {
            values[i] = entity.getValues()[i];
        }
        values[values.length - 1] = Integer.toString(entity.getId());

        return executeUpdate(sql, values);
    }

    public boolean delete(int id) {
        String sql = getSqlBuilder()
                .deleteFrom(table)
                .where(Columns.ID + " = ?")
                .build();

        return execute(sql, new String[]{Integer.toString(id)});
    }

    SqlBuilder getSqlBuilder() {
        return new SqlBuilder();
    }

    private String getWhereClauseFor(String[] columns) {
        StringBuilder stringBuilder = new StringBuilder(columns[0])
                .append(" = ?");
        for (int i = 1; i < columns.length; i++) {
            stringBuilder.append(" AND ")
                    .append(columns[i])
                    .append(" = ?");
        }

        return stringBuilder.toString();
    }

    PreparedStatement getPreparedStatement(String sql) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return statement;
    }

    private boolean execute(String sql, String[] values) {
        boolean executed = false;
        try (PreparedStatement statement = getPreparedStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setNString(i + 1, values[i]);
            }

            executed = statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return executed;
    }

    List<E> executeQuery(String sql, String[] values) {
        List<E> entityList = new ArrayList<>();
        try (PreparedStatement statement = getPreparedStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setNString(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) entityList.add(parseEntityFrom(resultSet));
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entityList;
    }

    private int executeUpdate(String sql, String[] values) {
        int result = 0;
        try (PreparedStatement statement = getPreparedStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setNString(i + 1, values[i]);
            }
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    static class Columns {
        static final String ALL = "*";
        static final String ID = "id";
    }
}
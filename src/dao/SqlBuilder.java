package dao;

class SqlBuilder {
    private StringBuilder sql = new StringBuilder();

    SqlBuilder insertInto(String table) {
        sql.append("INSERT INTO ")
                .append(table);

        return this;
    }

    SqlBuilder update(String table) {
        sql.append("UPDATE ")
                .append(table);

        return this;
    }

    SqlBuilder deleteFrom(String table) {
        sql.append("DELETE ");

        return from(table);
    }

    SqlBuilder set(String[] columns) {
        sql.append(" SET ")
                .append(columns[0])
                .append(" = ")
                .append("?");
        for (int i = 1; i < columns.length; i++) {
            sql.append(", ")
                    .append(columns[i])
                    .append(" = ")
                    .append("?");
        }

        return this;
    }

    SqlBuilder values(String... values) {
        sql.append(" VALUES ")
                .append("(");
        appendValues(values);
        sql.append(")");

        return this;
    }

    SqlBuilder select(String... columns) {
        sql.append("SELECT ");
        sql.append(columns[0]);
        for (int i = 1; i < columns.length; i++) {
            sql.append(", ").append(columns[i]);
        }

        return this;
    }

    SqlBuilder from(String table) {
        sql.append(" FROM ").append(table);

        return this;
    }

    SqlBuilder where(String whereClause) {
        sql.append(" WHERE ").append(whereClause);

        return this;
    }

    String build() {
        return sql.toString();
    }

    private void appendValues(String[] values) {
        sql.append("?");
        for (int i = 1; i < values.length; i++) {
            sql.append(", ").append("?");
        }
    }
}
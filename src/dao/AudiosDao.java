package dao;

import model.Audio;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AudiosDao extends AbstractDao<Audio> {
    private static final String TABLE = "audios";

    public AudiosDao() {
        super(TABLE);
    }

    @Override
    protected Audio parseEntityFrom(ResultSet resultSet) throws SQLException {
        return new Audio(
                resultSet.getInt(1),
                resultSet.getInt(2),
                resultSet.getString(3),
                resultSet.getString(4)
        );
    }
}

package servlet;

import freemarker.Render;
import model.LocalDirs;
import model.Pages;
import model.Parameters;
import model.User;
import service.SessionService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

@MultipartConfig
public class EditProfileServlet extends UploadServlet {
    private SessionService sessionService = SessionService.get();
    private UserService userService = UserService.get();

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = userService.getUserById(sessionService.getCurrentUserId(request));
        user.setUsername(request.getParameter(Parameters.USERNAME));
        user.setEmail(request.getParameter(Parameters.EMAIL));
        user.setCountry(request.getParameter(Parameters.COUNTRY));
        boolean canReceiveMsg = Boolean.parseBoolean(
                request.getParameter(Parameters.CAN_RECEIVE_MSG)
        );
        user.setCanReceiveMsg(canReceiveMsg);
        user.setDescription(request.getParameter(Parameters.DESCRIPTION));
        String photoPath = uploadUserPhoto(request);
        if (photoPath != null) user.setPhotoSrc(photoPath);
        userService.updateUser(user);

        response.sendRedirect(Pages.PROFILE);
    }

    private String uploadUserPhoto(HttpServletRequest request) throws ServletException, IOException {
        String src = null;

        Part part = request.getPart("userPhoto");
        if (part != null) {
            String dest = getPathTo(LocalDirs.USER_PHOTOS);
            String fileName = sessionService.getCurrentUserId(request) + "." + getFileExtension(part.getSubmittedFileName());
            uploadFile(part, dest, fileName);
            src = LocalDirs.USER_PHOTOS + File.separator + fileName;
        }

        return src;
    }

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        User user = userService.getUserById(sessionService.getCurrentUserId(request));

        HashMap<String, Object> data = new HashMap<>(2);
        data.put("userId", user.getId());
        data.put("profile", user);

        Render.rend(request, responseWriter, "edit_profile.ftl", data);
    }
}

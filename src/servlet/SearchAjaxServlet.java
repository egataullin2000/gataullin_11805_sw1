package servlet;

import com.google.gson.Gson;
import model.Album;
import model.DataList;
import model.Parameters;
import service.SearchService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class SearchAjaxServlet extends HttpServlet {
    private SearchService service = SearchService.get();

    private Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String query = request.getParameter(Parameters.QUERY);
        DataList<Album> data = new DataList<>(
                service.search(query)
        );

        response.setContentType("text/json");
        try (PrintWriter writer = response.getWriter()) {
            writer.write(gson.toJson(data));
        }
    }
}

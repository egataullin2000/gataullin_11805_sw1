package servlet;

import freemarker.Render;
import model.*;
import service.AlbumService;
import service.SessionService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@MultipartConfig
public class AddAlbumServlet extends UploadServlet {
    private SessionService sessionService = SessionService.get();
    private UserService userService = UserService.get();
    private AlbumService albumService = AlbumService.get();

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int userId = sessionService.getCurrentUserId(request);
        String albumTitle = request.getParameter(Parameters.TITLE);
        String genre = request.getParameter(Parameters.GENRE);
        int year = Integer.parseInt(
                request.getParameter(Parameters.YEAR)
        );
        String desc = request.getParameter(Parameters.DESCRIPTION);

        Album album = new Album(userId, albumTitle, genre, year, desc);
        albumService.addAlbum(album);
        album = albumService.getAlbumByFields(
                new String[]{Album.FIELDS[1], Album.FIELDS[2]},
                new String[]{Integer.toString(userId), albumTitle}
        );

        System.out.println(Arrays.toString(album.getValues()));

        String imagePath = uploadAlbumImage(request.getPart("poster"), album.getId());
        album.setPosterSrc(imagePath);

        imagePath = uploadAlbumImage(request.getPart("background"), album.getId());
        album.setBackgroundSrc(imagePath);

        List<Audio> audios = uploadAudios(request, album.getId());
        album.setAudios(audios);

        System.out.println(Arrays.toString(album.getValues()));
        albumService.updateAlbum(album);

        response.sendRedirect(Pages.ALBUM + "?id=" + album.getId());
    }

    String uploadAlbumImage(Part part, int albumId) throws IOException {
        String path = null;

        if (part != null) {
            String dest = getPathTo(LocalDirs.ALBUM_IMAGES);
            String extension = getFileExtension(part.getSubmittedFileName());
            String fileName = albumId + "_" + part.getName() + "." + extension;
            uploadFile(part, dest, fileName);
            path = LocalDirs.ALBUM_IMAGES + File.separator + fileName;
            System.out.println(path);
        }

        return path;
    }

    List<Audio> uploadAudios(HttpServletRequest request, int albumId) throws ServletException, IOException {
        List<Audio> audios = new LinkedList<>();
        Collection<Part> parts = request.getParts();
        for (Part part : parts) {
            System.out.println(part.getName());
            if (part.getName().equals("audio")) {
                String[] submittedNameData = part.getSubmittedFileName().split("\\.");
                String fileName = albumId + "_" + (audios.size() + 1) + "."
                        + submittedNameData[submittedNameData.length - 1];
                audios.add(new Audio(
                        albumId,
                        submittedNameData[0],
                        uploadAudio(part, fileName)
                ));
            }
        }

        return audios;
    }

    private String uploadAudio(Part part, String fileName) throws IOException {
        String dest = getPathTo(LocalDirs.AUDIOS);
        uploadFile(part, dest, fileName);

        return LocalDirs.AUDIOS + File.separator + fileName;
    }

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        Map<String, Object> data = new HashMap<>(2);

        int userId = sessionService.getCurrentUserId(request);
        data.put("userId", userId);
        data.put("user", userService.getUserById(userId));

        Render.rend(request, responseWriter, "add_album.ftl", data);
    }
}

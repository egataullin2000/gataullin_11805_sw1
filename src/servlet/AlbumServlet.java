package servlet;

import freemarker.Render;
import model.Album;
import model.Parameters;
import service.AlbumService;
import service.SessionService;
import service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class AlbumServlet extends Servlet {
    private SessionService sessionService = SessionService.get();
    private UserService userService = UserService.get();
    private AlbumService albumService = AlbumService.get();

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        int albumId = Integer.parseInt(request.getParameter(Parameters.ID));
        Album album = albumService.getAlbumById(albumId);

        Map<String, Object> data = new HashMap<>(3);
        data.put("userId", sessionService.getCurrentUserId(request));
        data.put("album", album);
        data.put("author", userService.getUserById(album.getPerformerId()));

        Render.rend(request, responseWriter, "album.ftl", data);
    }
}

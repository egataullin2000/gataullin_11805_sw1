package servlet;

import freemarker.Render;
import model.Album;
import model.Parameters;
import service.AlbumService;
import service.SessionService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@MultipartConfig
public class EditAlbumServlet extends AddAlbumServlet {
    private SessionService sessionService = SessionService.get();
    private AlbumService albumService = AlbumService.get();

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Album album = getAlbum(request);
        album.setTitle(request.getParameter(Parameters.TITLE));
        String imageSrc = uploadAlbumImage(request.getPart("poster"), album.getId());
        if (imageSrc != null) album.setPosterSrc(imageSrc);
        imageSrc = uploadAlbumImage(request.getPart("background"), album.getId());
        album.setBackgroundSrc(imageSrc);
        album.setGenre(request.getParameter(Parameters.GENRE));
        album.setYear(Integer.parseInt(
                request.getParameter(Parameters.YEAR)
        ));
        album.getAudios().addAll(uploadAudios(request, album.getId()));

        albumService.updateAlbum(album);
    }

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        Map<String, Object> data = new HashMap<>(2);
        data.put("userId", sessionService.getCurrentUserId(request));
        data.put("album", getAlbum(request));

        Render.rend(request, responseWriter, "edit_album.ftl", data);
    }

    private Album getAlbum(HttpServletRequest request) {
        return albumService.getAlbumById(
                Integer.parseInt(request.getParameter(Parameters.ID))
        );
    }
}

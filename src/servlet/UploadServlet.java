package servlet;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;

@MultipartConfig
public abstract class UploadServlet extends Servlet {
    String getPathTo(String localDir) {
        return getServletContext().getRealPath("") + localDir;
    }

    String getFileExtension(String fileName) {
        String[] fileNameData = fileName.split("\\.");
        return fileNameData[fileNameData.length - 1];
    }

    void uploadFile(Part filePart, String dest, String fileName) throws IOException {
        String path = dest + File.separator + fileName;
        File file = new File(dest);
        if (!file.exists()) {
            file.mkdir();
        } else {
            file = new File(path);
            if (file.exists()) file.delete();
        }

        filePart.write(path);
    }
}

package servlet;

import model.Parameters;
import model.User;
import service.EmailService;
import service.SessionService;
import service.UserService;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SendMessageServlet extends HttpServlet {
    private SessionService sessionService = SessionService.get();
    private UserService userService = UserService.get();
    private EmailService emailService = EmailService.get();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("Sending msg");
        User user = userService.getUserById(sessionService.getCurrentUserId(request));
        int profileId = Integer.parseInt(
                request.getParameter(Parameters.ID)
        );
        User profile = userService.getUserById(profileId);

        try {
            emailService.sendMessage(
                    profile.getEmail(),
                    "Letter from @" + user.getUsername(),
                    request.getParameter(Parameters.TEXT)
            );
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}

package servlet;

import freemarker.Render;
import model.Parameters;
import model.User;
import service.SessionService;
import service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class ProfileServlet extends Servlet {
    private SessionService sessionService = SessionService.get();
    private UserService userService = UserService.get();

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        Map<String, Object> data = new HashMap<>(2);

        int id = sessionService.getCurrentUserId(request);
        data.put("userId", id);

        String sid = request.getParameter(Parameters.ID);
        id = sid == null ? id : Integer.parseInt(sid);
        User profile = userService.getUserById(id);

        data.put("profile", profile);

        Render.rend(request, responseWriter, "profile.ftl", data);
    }
}

package servlet;

import freemarker.Render;
import model.CookieAttributes;
import model.Pages;
import model.Parameters;
import model.SessionAttributes;
import service.AuthService;
import service.SessionService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class LoginServlet extends Servlet {
    private SessionService sessionService = SessionService.get();
    private AuthService authService = AuthService.get();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (tryAuth(request, response)) {
            response.sendRedirect(Pages.PROFILE);
        } else response.sendRedirect(Pages.LOGIN);
    }

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        Map<String, Object> data = new HashMap<>(1);
        data.put("userId", sessionService.getCurrentUserId(request));

        Render.rend(request, responseWriter, "login.ftl", data);
    }

    private boolean tryAuth(HttpServletRequest request, HttpServletResponse response) {
        String login = request.getParameter(Parameters.LOGIN);
        String password = request.getParameter(Parameters.PASSWORD);
        int userId = authService.authenticate(login, password);
        if (userId != -1) {
            request.getSession().setAttribute(SessionAttributes.CURRENT_USER_ID, userId);
            if (Boolean.parseBoolean(request.getParameter(Parameters.REMEMBER_ME))) {
                response.addCookie(new Cookie(CookieAttributes.USER_ID, Integer.toString(userId)));
            }

            return true;
        }

        return false;
    }
}

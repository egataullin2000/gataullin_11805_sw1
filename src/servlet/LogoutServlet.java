package servlet;

import model.CookieAttributes;
import model.Pages;
import model.SessionAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().setAttribute(SessionAttributes.CURRENT_USER_ID, -1);
        response.addCookie(new Cookie(CookieAttributes.USER_ID, "-1"));
        response.sendRedirect(Pages.MAIN);
    }
}

package servlet;

import freemarker.Render;
import model.Album;
import model.User;
import service.AlbumService;
import service.SessionService;
import service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainServlet extends Servlet {
    private SessionService sessionService = SessionService.get();
    private UserService userService = UserService.get();
    private AlbumService albumService = AlbumService.get();

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        List<User> lastPerformers = userService.getLastPerformers(8);
        List<Album> lastAlbums = albumService.getLast(8);

        Map<String, Object> data = new HashMap<>(3);
        data.put("userId", sessionService.getCurrentUserId(request));
        data.put("performers", lastPerformers);
        data.put("albums", lastAlbums);

        Render.rend(request, responseWriter, "main.ftl", data);
    }
}

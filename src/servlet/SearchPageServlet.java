package servlet;

import freemarker.Render;
import model.Parameters;
import service.SessionService;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class SearchPageServlet extends Servlet {
    private SessionService sessionService = SessionService.get();

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        String query = request.getParameter(Parameters.QUERY);

        Map<String, Object> data = new HashMap<>(2);
        data.put("userId", sessionService.getCurrentUserId(request));
        data.put("query", query);

        Render.rend(request, responseWriter, "search.ftl", data);
    }
}

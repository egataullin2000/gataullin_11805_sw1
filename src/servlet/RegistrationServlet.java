package servlet;

import freemarker.Render;
import model.CookieAttributes;
import model.Pages;
import model.Parameters;
import model.User;
import service.RegistrationService;
import service.SessionService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class RegistrationServlet extends Servlet {
    private SessionService sessionService = SessionService.get();
    private RegistrationService registrationService = RegistrationService.get();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        boolean isPerformer = Boolean.parseBoolean(request.getParameter(Parameters.IS_PERFORMER));
        String username = request.getParameter(Parameters.USERNAME);
        String email = request.getParameter(Parameters.EMAIL);
        String password = request.getParameter(Parameters.PASSWORD);
        String country = request.getParameter(Parameters.COUNTRY);

        User user = new User(
                isPerformer,
                username,
                email,
                country
        );
        int id = registrationService.register(user, password);
        if (id > 0) {
            response.addCookie(new Cookie(CookieAttributes.USER_ID, Integer.toString(id)));
            boolean isArtist = Boolean.parseBoolean(request.getParameter(Parameters.IS_PERFORMER));
            response.sendRedirect(isArtist ? Pages.EDIT_PROFILE : Pages.MAIN);
        } else response.sendRedirect(Pages.REGISTRATION);
    }

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        Map<String, Object> data = new HashMap<>(1);
        data.put("userId", sessionService.getCurrentUserId(request));

        Render.rend(request, responseWriter, "registration.ftl", data);
    }
}

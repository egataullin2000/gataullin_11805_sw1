package servlet;

import freemarker.Render;
import model.Pages;
import model.Parameters;
import service.AuthService;
import service.EmailService;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class SendCodeServlet extends Servlet {
    private AuthService authService = AuthService.get();
    private EmailService emailService = EmailService.get();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String email = req.getParameter(Parameters.EMAIL);
        sendCode(email, authService.generateRecoveryCode(email));
        resp.sendRedirect(Pages.RESTORE + "?email=" + email);
    }

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        Map<String, Object> data = new HashMap<>(1);
        data.put("userId", -1);

        Render.rend(request, responseWriter, "send_code.ftl", data);
    }

    private void sendCode(String email, String code) {
        try {
            emailService.sendMessage(
                    email,
                    "Password recovery code",
                    "Your code is " + code
            );
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}

package servlet;

import freemarker.Render;
import model.Pages;
import model.Parameters;
import service.AuthService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class RestoreServlet extends Servlet {
    private AuthService service = AuthService.get();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String email = request.getParameter(Parameters.EMAIL);
        String code = request.getParameter(Parameters.CODE);
        String password = request.getParameter(Parameters.PASSWORD);
        if (service.checkRecoveryCode(email, code)) {
            service.resetPassword(email, password);
            response.sendRedirect(Pages.LOGIN);
        } else response.sendRedirect(Pages.RESTORE);
    }

    @Override
    public void rend(HttpServletRequest request, PrintWriter responseWriter) {
        Map<String, Object> data = new HashMap<>(2);
        data.put("userId", -1);
        data.put("email", request.getParameter(Parameters.EMAIL));

        Render.rend(request, responseWriter, "restore_password.ftl", data);
    }
}

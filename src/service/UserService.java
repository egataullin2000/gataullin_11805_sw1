package service;

import dao.UsersDao;
import model.User;

import java.util.List;

public class UserService {
    private static UserService instance;

    private UsersDao dao;

    private UserService() {
        dao = new UsersDao();
    }

    public static UserService get() {
        if (instance == null) instance = new UserService();

        return instance;
    }

    public void addUser(User user) {
        dao.insert(user);
    }

    public User getUserById(int id) {
        return dao.getEntityById(id);
    }

    public User getUserByEmail(String email) {
        return dao.getEntityWith(new String[]{"email"}, new String[]{email});
    }

    public void updateUser(User user) {
        dao.update(user);
    }

    public List<User> search(String query) {
        return dao.getEntities("username LIKE %\\?%", query);
    }

    public List<User> getLastPerformers(int count) {
        List<User> performers = dao.getEntitiesWith(
                new String[]{"isPerformer"},
                new String[]{"true"}
        );
        if (performers.size() > count) {
            performers = performers.subList(performers.size() - 1 - count, performers.size() - 1);
        }

        return performers;
    }
}

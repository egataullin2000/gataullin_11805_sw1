package service;

import dao.AuthDao;
import model.AuthData;
import model.CookieAttributes;
import model.SessionAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

public class AuthService {
    private static AuthService instance;

    private AuthDao dao;

    private AuthService() {
        dao = new AuthDao();
    }

    public static AuthService get() {
        if (instance == null) instance = new AuthService();

        return instance;
    }

    public int tryAuth(HttpServletRequest request) {
        int id = -1;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            id = authenticate(cookies);
            if (id != -1) request.getSession().setAttribute(SessionAttributes.CURRENT_USER_ID, id);
        }

        return id;
    }

    public int authenticate(String login, String password) {
        AuthData authData = dao.getEntityByLogin(login);

        if (authData != null) {
            System.out.println(authData);
            return authData.hashCode() == AuthData.hashCodeOf(authData.getSalt(), password) ? authData.getId() : -1;
        } else {
            System.out.println("No such user");
            return -1;
        }
    }

    public int authenticate(Cookie[] cookies) {
        for (Cookie cookie : cookies) {
            if (CookieAttributes.USER_ID.equals(cookie.getName())) {
                return Integer.parseInt(cookie.getValue());
            }
        }

        return -1;
    }

    public boolean checkRecoveryCode(String login, String code) {
        AuthData authData = dao.getEntityByLogin(login);
        boolean isCodeTrue = code.equals(authData.getRecoveryCode());
        if (isCodeTrue) {
            authData.setRecoveryCode(null);
            dao.update(authData);
        }

        return isCodeTrue;
    }

    public void resetPassword(String email, String password) {
        AuthData authData = dao.getEntityByLogin(email);
        authData.setPassword(password);
        dao.update(authData);
    }

    public boolean isLoginFree(String login) {
        return dao.getEntityByLogin(login) != null;
    }

    public void addAuthData(String username, String email, String password) {
        AuthData authData = new AuthData(username, email, password);
        dao.insert(authData);
    }

    public String generateRecoveryCode(String email) {
        AuthData authData = dao.getEntityByLogin(email);
        authData.setRecoveryCode(UUID.randomUUID().toString());
        dao.update(authData);

        return authData.getRecoveryCode();
    }
}

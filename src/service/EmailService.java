package service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailService {
    private static final String SERVICE_MAIL_ADDRESS = "11ojlehellia2000@gmail.com";
    private static final String SERVICE_MAIL_PASSWORD = "11Edgar2000";

    private static EmailService instance;

    private Session session;

    private EmailService() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        session = Session.getInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(SERVICE_MAIL_ADDRESS, SERVICE_MAIL_PASSWORD);
            }
        });
    }

    public static EmailService get() {
        if (instance == null) instance = new EmailService();

        return instance;
    }

    public void sendMessage(String email, String subject, String text) throws MessagingException {
        System.out.println("Data: " + email + " " + subject + " " + text);
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(SERVICE_MAIL_ADDRESS));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
        message.setSubject(subject);
        message.setText(text);

        Transport.send(message);
    }
}

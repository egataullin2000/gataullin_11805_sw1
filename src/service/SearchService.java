package service;

import model.Album;

import java.util.List;

public class SearchService {
    private static SearchService instance;

    private AlbumService albumService;

    private SearchService() {
        albumService = AlbumService.get();
    }

    public static SearchService get() {
        if (instance == null) instance = new SearchService();

        return instance;
    }

    public List<Album> search(String query) {
        return albumService.search(query);
    }
}

package service;

import dao.AlbumsDao;
import dao.AudiosDao;
import model.Album;
import model.Audio;

import java.util.List;

public class AlbumService {
    private static AlbumService instance;

    private AlbumsDao albumsDao;
    private AudiosDao audiosDao;

    private AlbumService() {
        albumsDao = new AlbumsDao();
        audiosDao = new AudiosDao();
    }

    public static AlbumService get() {
        if (instance == null) instance = new AlbumService();

        return instance;
    }

    public void addAlbum(Album album) {
        albumsDao.insert(album);
    }

    public void addAudios(List<Audio> audios) {
        for (Audio audio : audios) audiosDao.insert(audio);
    }

    public Album getAlbumById(int id) {
        return albumsDao.getEntityById(id);
    }

    public Album getAlbumByPerformer(int performerId) {
        return albumsDao.getEntityWith(
                new String[]{Album.FIELDS[1]},
                new String[]{Integer.toString(performerId)}
        );
    }

    public Album getAlbumByFields(String[] fields, String[] values) {
        return albumsDao.getEntityWith(fields, values);
    }

    public void updateAlbum(Album album) {
        albumsDao.update(album);
        addAudios(album.getAudios());
    }

    public List<Album> search(String query) {
        return albumsDao.search(query);
    }

    public List<Album> getLast(int count) {
        List<Album> albums = albumsDao.getAll();
        if (albums.size() > count) {
            albums = albums.subList(albums.size() - 1 - count, albums.size() - 1);
        }

        return albums;
    }
}

package model;

public abstract class Entity {
    protected int id;

    public int getId() {
        return id;
    }

    public abstract String[] getFields();

    public abstract String[] getValues();
}

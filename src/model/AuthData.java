package model;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class AuthData extends Entity {
    private String username;
    private String email;
    private String salt;
    private int hash;
    private String recoveryCode;

    public AuthData(String username, String email, String password) {
        this.username = username;
        this.email = email;
        salt = UUID.randomUUID().toString();
        setPassword(password);
        recoveryCode = UUID.randomUUID().toString();
    }

    public AuthData(int id, String username, String email, String salt, int hash, String recoveryCode) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.salt = salt;
        this.hash = hash;
        this.recoveryCode = recoveryCode;
    }

    public static int hashCodeOf(String salt, String password) {
        int hash = 0;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] data = messageDigest.digest((salt + password).getBytes());
            hash = ByteBuffer.wrap(data).getInt();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return hash;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getSalt() {
        return salt;
    }

    public void setPassword(String password) {
        hash = hashCodeOf(salt, password);
    }

    public String getRecoveryCode() {
        return recoveryCode;
    }

    public void setRecoveryCode(String code) {
        recoveryCode = code;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public String[] getFields() {
        return new String[]{
                "id",
                "username",
                "email",
                "salt",
                "hash",
                "recoveryCode"
        };
    }

    @Override
    public String[] getValues() {
        return new String[]{
                Integer.toString(id),
                username,
                email,
                salt,
                Integer.toString(hash),
                recoveryCode
        };
    }
}

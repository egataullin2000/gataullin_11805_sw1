package model;

public class Parameters {
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String CONFIRM = "confirm";
    public static final String COUNTRY = "country";
    public static final String IS_PERFORMER = "isPerformer";
    public static final String REMEMBER_ME = "rememberMe";
    public static final String LOGIN = "login";
    public static final String CODE = "code";
    public static final String ID = "id";
    public static final String CAN_RECEIVE_MSG = "canReceiveMsg";
    public static final String DESCRIPTION = "description";
    public static final String TITLE = "title";
    public static final String GENRE = "genre";
    public static final String YEAR = "year";
    public static final String TEXT = "text";
    public static final String TYPE = "type";
    public static final String QUERY = "query";
}

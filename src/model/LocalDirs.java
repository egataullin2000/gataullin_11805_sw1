package model;

public class LocalDirs {
    public static final String USER_PHOTOS = "UserPhotos";
    public static final String ALBUM_IMAGES = "AlbumImages";
    public static final String AUDIOS = "Audios";
}

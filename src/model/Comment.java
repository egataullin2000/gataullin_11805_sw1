package model;

import java.util.Calendar;

public class Comment extends Entity {
    public static final String[] FIELDS = new String[]{"id", "albumId", "commentatorId", "text", "date"};

    private int albumId;
    private User commentator;
    private String text;
    private Calendar date;

    public Comment(User commentator, String text) {
        this.commentator = commentator;
        this.text = text;
    }

    public Comment(int id, int albumId, User commentator, String text, Calendar date) {
        this(commentator, text);
        this.id = id;
        this.albumId = albumId;
        this.date = date;
    }

    public int getAlbumId() {
        return albumId;
    }

    public User getCommentator() {
        return commentator;
    }

    public String getText() {
        return text;
    }

    public Calendar getDate() {
        return date;
    }

    @Override
    public String[] getFields() {
        return FIELDS;
    }

    @Override
    public String[] getValues() {
        return new String[]{
                Integer.toString(id),
                Integer.toString(albumId),
                Integer.toString(commentator.getId()),
                text,
                date.toString()
        };
    }
}

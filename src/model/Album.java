package model;

import java.util.List;

public class Album extends Entity {
    public static final String[] FIELDS = new String[]{
            "id",
            "performerId",
            "title",
            "posterSrc",
            "backgroundSrc",
            "genre",
            "year",
            "description"
    };

    private int performerId;
    private String title;
    private String posterSrc;
    private String backgroundSrc;
    private String description;
    private String genre;
    private int year;
    private List<Audio> audios;
    private List<Comment> comments;

    public Album(int performerId, String title, String genre, int year, String description) {
        this.performerId = performerId;
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.description = description;
    }

    public Album(int id, int performerId, String title, String posterSrc, String backgroundSrc, String genre, int year, String description) {
        this(performerId, title, genre, year, description);
        this.id = id;
        this.posterSrc = posterSrc;
        this.backgroundSrc = backgroundSrc;
    }

    public int getPerformerId() {
        return performerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosterSrc() {
        return posterSrc;
    }

    public void setPosterSrc(String posterSrc) {
        this.posterSrc = posterSrc;
    }

    public String getBackgroundSrc() {
        return backgroundSrc;
    }

    public void setBackgroundSrc(String backgroundSrc) {
        this.backgroundSrc = backgroundSrc;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public List<Audio> getAudios() {
        return audios;
    }

    public void setAudios(List<Audio> audios) {
        this.audios = audios;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Comment> getComments() {
        return comments;
    }

    @Override
    public String[] getFields() {
        return FIELDS;
    }

    @Override
    public String[] getValues() {
        return new String[]{
                Integer.toString(id),
                Integer.toString(performerId),
                title,
                posterSrc,
                backgroundSrc,
                genre,
                Integer.toString(year),
                description
        };
    }
}

package model;

import java.util.List;

public class User extends Entity {
    private boolean isPerformer;
    private String username;
    private String email;
    private String country;
    private String photoSrc;
    private boolean canReceiveMsg;
    private String description;
    private List<Album> albums;

    public User(boolean isPerformer, String username, String email, String country) {
        this.isPerformer = isPerformer;
        this.username = username;
        this.email = email;
        this.country = country;
    }

    public User(int id, boolean isPerformer, String username, String email, String country, boolean canReceiveMsg) {
        this(isPerformer, username, email, country);
        this.id = id;
        this.canReceiveMsg = canReceiveMsg;
    }

    public void setCanReceiveMsg(boolean canReceiveMsg) {
        this.canReceiveMsg = canReceiveMsg;
    }

    public boolean isPerformer() {
        return isPerformer;
    }

    public void setPerformer(boolean performer) {
        isPerformer = performer;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhotoSrc() {
        return photoSrc;
    }

    public void setPhotoSrc(String photoSrc) {
        this.photoSrc = photoSrc;
    }

    public boolean canReceiveMsg() {
        return canReceiveMsg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    @Override
    public boolean equals(Object object) {
        return object instanceof User && ((User) object).id == id;
    }

    @Override
    public String[] getFields() {
        return new String[]{
                "id",
                "isPerformer",
                "username",
                "email",
                "country",
                "photoSrc",
                "canReceiveMsg",
                "description"
        };
    }

    @Override
    public String[] getValues() {
        return new String[]{
                Integer.toString(id),
                Boolean.toString(isPerformer),
                username,
                email,
                country,
                photoSrc,
                Boolean.toString(canReceiveMsg),
                description
        };
    }
}

package model;

public class Pages {
    public static final String REGISTRATION = "/join";
    public static final String LOGIN = "/login";
    public static final String RESTORE = "/restore";
    public static final String MAIN = "/";
    public static final String PROFILE = "/profile";
    public static final String EDIT_PROFILE = PROFILE + "/edit";
    public static final String ALBUM = "/album";
    public static final String ADD_ALBUM = PROFILE + ALBUM + "/new";
}
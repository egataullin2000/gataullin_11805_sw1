package model;

public class Audio extends Entity {
    public static final String[] FIELDS = new String[]{"id", "albumId", "title", "src"};

    private int albumId;
    private String title;
    private String src;

    public Audio(int id, int albumId, String title, String src) {
        this(albumId, title, src);
        this.id = id;
    }

    public Audio(int albumId, String title, String src) {
        this.albumId = albumId;
        this.title = title;
        this.src = src;
    }

    public int getAlbumId() {
        return albumId;
    }

    public String getTitle() {
        return title;
    }

    public String getSrc() {
        return src;
    }

    @Override
    public String[] getFields() {
        return FIELDS;
    }

    @Override
    public String[] getValues() {
        return new String[]{
                Integer.toString(id),
                Integer.toString(albumId),
                title,
                src
        };
    }
}

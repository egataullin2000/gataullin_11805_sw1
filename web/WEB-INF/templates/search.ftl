<!DOCTYPE html>
<html lang="en">
<head>
    <title>${query}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/css/menu.css" type="text/css">
</head>
<body>
<nav class="navbar navbar-dark bg-info">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand bg-light" href="/">MusicBase</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">Home</a></li>
            <li><a href="/profile">My Profile</a></li>
        </ul>
        <form class="navbar-form navbar-left" action="/search">
            <div class="input-group">
                <input class="form-control" name="query" type="text" id="query"
                        <#if !$query??>
                            value="${query}"
                        </#if>
                       class="who" placeholder="Search" autofocus>
                <div class="input-group-btn">
                    <button class="btn btn-default" onclick="f()">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </div>
            </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <#if userId == -1>
                <li><a href="/join"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            <#else>
                <form action="/logout" method="post">
                    <button type="submit"><a><span class="glyphicon glyphicon-log-in"></span> Logout</a></button>
                </form>
            </#if>
        </ul>
    </div>
</nav>
<div id="res">
    <script type="application/javascript">
        $(document).ready(f());

        function f() {
            if ($("#query").val().length >= 1) {
                $.ajax({
                    url: "/search/ajax",
                    data: {"query": $("#query").val()},
                    dataType: "json",
                    success: function (msg) {
                        if (msg.data.length > 0) {
                            $("#res").html("");
                            for (var i = 0; i < msg.data.length; i++) {
                                printItem(msg.data[i])
                            }
                        } else {
                            $("#res").html("No results..");
                        }
                    }
                })
            } else {
                $("#res").html("");
            }
        }

        function printItem(item) {
            $("#res").append("<div class=\"card\">" +
                "<img src=\"" + item.posterSrc + "\" alt=\"" + item.title + "\" style=\"width: 200px; margin: 16px\">" +
                "<p><a href=\"/album?id=" + item.id + "\" <h3>" + item.title + "</h3></a></p>" +
                "</div>");
        }
    </script>
</div>
</body>
</html>
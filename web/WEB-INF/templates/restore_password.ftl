<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Recovery</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/recovery_style.css" type="text/css">
</head>
<body>
<#include "menu.ftl">
<script>
    function validateFormRecovery() {
// проверяем пароли
// выбираем элементы
        var password1 = document.getElementById('psw');
        var password2 = document.getElementById('psw1');
// сравниваем написанное, если не равно, то:
        if (password1.value !== password2.value) {
// сообщаем пользователю, можно сделать как угодно
            alert('Проверьте пароли!');
// сообщаем браузеру, что не надо обрабатывать нажатие кнопки
// как обычно, т. е. не надо отправлять форму
            return false;
        }
        var password_regex = /[0-9a-zа-я_A-ZА-Я]*[A-Z]+[0-9a-zа-яА-ЯA-Z]*/i;
        if (!(password1.value.length >= 8 && password_regex.test(password1.value))) {
            alert('Слабый пароль')
        }
    }
</script>
<div class="recovery">
    <div class="recovery_form">
        <form class="recovery_form_form" method="post">
            <h1>Recovery</h1>
            <div class="box-input">
                <input class="input" name="email" type="text"
                        <#if !$email??>
                            value="${email}"
                        </#if>
                       required>
                <label>Email</label>
            </div>
            <div class="box-input">
                <input class="input" name="code" type="code" required>
                <label>Code</label>
            </div>
            <div class="box-input">
                <input name="password" class="input" type="password" id="psw" required>
                <label>New Password</label>
            </div>
            <div class="box-input">
                <input name="confirm" class="input" type="password" id="psw1" required>
                <label>Confirm password</label>
            </div>
            <button type="submit">Reset password</button>
        </form>
    </div>
</div>
</body>
</html>
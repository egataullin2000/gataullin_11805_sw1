<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>New album</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="/css/album_edit.css" type="text/css">
</head>
<script>
    $("#form-load-image").dmUploader({
        url: "upload.php",
        onBeforeUpload: function (id) {
            $(".message-load").text("Uploading image");
        },
        onUploadSuccess: function (id, data) {
            if (data) {
                var fullPathToImg = location.protocol + "//" + location.hostname + "/" + data;
                $("#target").attr("src", fullPathToImg);
            } else {
                alert("Error uploading file");
            }
            $(".message-load").text("");
        }
    });
</script>
<script type="text/javascript">
    function clicked() {
        alert('Are you sure?');
    }
</script>
<body>
<#include "menu.ftl">
<section>
    <form method="post" enctype="multipart/form-data" class="album-info">
        <div class="album-art">
            <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png"/>
            <input id="file-load-field" name="poster" type="file" title="Album's poster">
            <input id="file-load-field" name="background" type="file" title="Background image">
            <p class="message-load"></p>
        </div>
        <div class="album-details">
            <h2>${user.username}</h2>
            <h1><input type="text" class="form-control" name="title" id="first_name"
                       placeholder="Title" title="enter your username if any."></h1>
            <span>
                <span>
                    <input type="text" class="form-control" name="genre" id="first_name"
                           placeholder="Genre" title="enter your username if any.">
                </span>
                <span>&copy;<input type="text" class="form-control" name="year" id="first_name"
                                   placeholder="Year" title="enter your username if any.">
                </span>
            </span>
            <p><input type="text" class="form-control" name="description" id="first_name"
                      placeholder="Description" title="enter your username if any."></p>
        </div>
        <div class="album-tracks">
            <input multiple type="file" class="form-control" name="audio" id="first_name"
                   placeholder="album" title="Upload your audios">
        </div>
        <button type="submit" class="play">Done</button>
    </form>
</section>
</body>
</html>
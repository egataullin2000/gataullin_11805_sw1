<!DOCTYPE html>
<html lang="en">
<head>
    <title>Main</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="css/home.css">
    <style>
        body, h1, h2, h3, h4, h5, h6 {
            font-family: "Roboto", sans-serif
        }

        .w3-bar-block .w3-bar-item {
            padding: 20px
        }
    </style>
</head>
<body>

<#include "menu.ftl">

<!-- !PAGE CONTENT! -->
<div class="w3-main w3-content w3-padding" style="max-width:1200px">

    <#if performers?size != 0>
        <!-- First Photo Grid-->
        <div class="w3-row-padding w3-padding-16 w3-center" id="artists">
            <h1>Performers</h1>

            <#list 0..3 as i>
                <#if performers[i]??>
                    <div class="w3-quarter">
                        <img src="${performers[i].photoSrc}" alt="${performers[i].username}"
                             style="width:200px">
                        <h3>${performers[i].username}</h3>
                    </div>
                </#if>
            </#list>
        </div>

        <!-- Second Photo Grid-->
        <div class="w3-row-padding w3-padding-16 w3-center">
            <#list 4..7 as i>
                <#if performers[i]??>
                    <div class="w3-quarter">
                        <img src="${performers[i].photoSrc}" alt="${performers[i].username}"
                             style="width:200px">
                        <h3>${performers[i].username}</h3>
                    </div>
                </#if>
            </#list>
        </div>
    </#if>

    <#if albums?size != 0>
        <!-- First Photo Grid-->
        <div class="w3-row-padding w3-padding-16 w3-center" id="music">
            <h1>Albums</h1>
            <#if albums??>
                <#list 0..3 as i>
                    <#if albums[i]??>
                        <div class="w3-quarter">
                            <img src="${albums[i].posterSrc}" alt="${albums[i].title}"
                                 style="width:200px">
                            <h3>${albums[i].title}</h3>
                        </div>
                    </#if>
                </#list>
            </#if>
            <#if albums??>
                <#list 4..7 as i>
                    <#if albums[i]??>
                        <div class="w3-quarter">
                            <img src="${albums[i].posterSrc}" alt="${albums[i].title}"
                                 style="width:200px">
                            <h3>${albums[i].title}</h3>
                        </div>
                    </#if>
                </#list>
            </#if>
        </div>

    </#if>
</div>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${album.title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="/css/album_edit.css" type="text/css">
</head>
<body>
<div class="background" style="background: ${album.backgroundSrc}"></div>
<section>
    <div class="album-info">
        <div class="album-art">
            <img src="${album.posterSrc}"/>
        </div>
        <div class="album-details">
            <h2><img src="${author.photoSrc}"/>${author.username}</h2>
            <h1>${album.title}</h1>
            <span>
                <span>${album.genre}</span>
                <span>
                    ${album.year}
                </span>
            </span>
            <p>${album.description}</p>
        </div>
    </div>
    <div class="album-tracks">
        <ol>
            <#list album.audios as audio>
                <li><span>${audio.title}</span><span><a href="${audio.src}">Listen online</a></span></li>
            </#list>
        </ol>
    </div>
    <div class="comments">
        <h3 class="title-comments">Comments</h3>
        <ul class="media-list">
            <#if album.comments?size != 0>
                <#list album.comments as comment>
                    <li class="media">
                        <div class="media-left">
                        </div>
                        <div class="media-body">
                            <div class="media-heading">
                                <div class="author">${comment.commentator.username}</div>
                                <div class="metadata">
                                    <span class="date">${comment.date}</span>
                                </div>
                            </div>
                            <div class="media-text text-justify">${comment.text}
                            </div>
                        </div>
                    </li>
                </#list>
            <#else>
                No comments
            </#if>
        </ul>
    </div>
</section>
</body>
</html>
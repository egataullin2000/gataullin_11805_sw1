<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign In</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/sign_in_style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<#include "menu.ftl">
<div class="sign_in">
    <div class="sign_in_form">
        <form class="sign_in_form_form" method="post">
            <h1>Sign in</h1>
            <div class="box-input">
                <input class="input" name="login" required>
                <label>Username</label>
            </div>
            <div class="box-input">
                <input class="input" type="password" name="password" required>
                <label>Password</label>
            </div>
            <div class="recover">
                <input type="checkbox" id="ckbox1" name="rememberMe" value="true">
                <label for="ckbox1"><span class="text">Remember me</span></label>
            </div>
            <button type="submit" class="button">Sign in</button>
            <a href="/restore/code/send">Lost your password?</a>
        </form>
    </div>
</div>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>${profile.username}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body, h1, h2, h3, h4, h5, h6 {
            font-family: "Roboto", sans-serif
        }

        .w3-bar-block .w3-bar-item {
            padding: 20px
        }
    </style>
    <link rel="stylesheet" href="/css/profile_edit.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
<script type="text/javascript">
    function sendMessage() {
        $.ajax({
            type: "POST",
            url: "/message/send",
            data: {
                "id": ${profile.id},
                "text": $("#message").val()
            },
            success: function () {
                alert("Done");
                $("#close").click();
            }
        })
    }
</script>
<#include "menu.ftl">
<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-sm-10"><h1>Profile</h1></div>
    </div>
    <div class="row">
        <div class="col-sm-3"><!--left col-->
            <div class="text-center">
                <img src="${profile.photoSrc}" class="avatar img-circle img-thumbnail"
                     alt="avatar">
                <h3></h3>
            </div>

            <#if profile.isPerformer()>
                <div class="panel panel-default">
                    <div class="panel-heading">Albums <i class="fa fa-link fa-1x"></i></div>
                    <#if profile.albums?size == 0>
                        <div class="panel-body">No released albums</div>
                    <#else>
                        <#list profile.albums as album>
                            <a href="/album?id=${album.id}" class="panel-body">${album.title}</a>
                        </#list>
                    </#if>
                    <#if userId == profile.id>
                        <a href="/profile/album/new" class="button" onclick="validateForm()">Add an album</a>
                    </#if>
                </div>
            </#if>

        </div><!--/col-3-->
        <div class="col-sm-9">

            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <form class="form" id="registrationForm">
                        <div class="form-group">
                            <label for="first_name"><h4>Username</h4></label>
                            <h3 name="first_name" id="first_name">${profile.username}</h3>
                        </div>
                        <div class="form-group">
                            <label for="email"><h4>Email</h4></label>
                            <h3 id="email">${profile.email}</h3>
                        </div>
                        <div class="form-group">
                            <label for="country"><h4>Country</h4></label>
                            <h3 id="country">${profile.country}</h3>
                        </div>
                        <#if userId == profile.id>
                            <div class="form-group">
                                <h5><a href="/profile/edit">Edit profile</a></h5>
                            </div>
                        </#if>
                        <#if userId != profile.id>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br>
                                    <#if userId != -1>
                                        <a href="#" type="button" class="button" data-toggle="modal"
                                           data-target="#myModal">Write a message</a>
                                    <#else>
                                        <a href="/login" type="button" class="button">Authenticate to write message</a>
                                    </#if>
                                </div>
                            </div>
                        </#if>
                    </form>

                    <div class="modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Modal body text goes here.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                </div><!--/tab-pane-->
            </div>

        </div><!--/tab-pane-->
    </div><!--/tab-content-->

</div><!--/col-9-->

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Message</h4>
            </div>
            <div class="modal-body">
                <p>
                    <textarea class="form-control" name="text" id="message"
                              title="enter your username if any."></textarea>
                </p>
            </div>
            <div class="modal-footer">
                <button id="close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default" onclick="sendMessage()">Send</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
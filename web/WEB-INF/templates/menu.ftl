<!DOCTYPE html>
<html lang="en">
<head>
    <title>Menu nav</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/css/menu.css" type="text/css">
</head>
<body>
<nav class="navbar navbar-dark bg-info">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand bg-light" href="/">MusicBase</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">Main</a></li>
            <li><a href="/profile">My Profile</a></li>
        </ul>
        <form class="navbar-form navbar-left" action="/search">
            <div class="input-group">
                <input name="query" type="text" class="form-control" placeholder="Search">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </div>
            </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <#if userId == -1>
                <li><a href="/join"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            <#else>
                <form action="/logout" method="post">
                    <button type="submit"><a><span class="glyphicon glyphicon-log-in"></span> Logout</a></button>
                </form>
            </#if>
        </ul>
    </div>
</nav>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit profile</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/profile_edit.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="jquery-3.4.1.min.js"></script>
</head>
<body>
<#include "menu.ftl">
<script type="text/javascript">
    function clicked() {
        alert('Are you sure?');
    }
</script>
<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-sm-10"><h1>Edit Profile</h1></div>
    </div>
    <form method="post" enctype="multipart/form-data" class="row">
        <div class="col-sm-3"><!--left col-->
            <div class="text-center">
                <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail"
                     alt="avatar">
                <h6>Upload a different photo...</h6>
                <input type="file" class="text-center center-block file-upload" name="userPhoto">
            </div>

            <#if profile.isPerformer()>
                <div class="panel panel-default">
                    <div class="panel-heading">Albums <i class="fa fa-link fa-1x"></i></div>
                    <#if profile.albums?size == 0>
                        <div class="panel-body">No released albums</div>
                    <#else>
                        <#list profile.albums as album>
                            <a href="/album?id=${album.id}" class="panel-body">${album.title}</a>
                        </#list>
                    </#if>
                    <#if userId == profile.id>
                        <a href="/profile/album/new" class="button" onclick="validateForm()">Add an album</a>
                    </#if>
                </div>
            </#if>

        </div><!--/col-3-->
        <div class="col-sm-9">

            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <div class="form" id="registrationForm">
                        <div class="form-group">
                            <label for="first_name"><h4>Username</h4></label>
                            <input type="text" class="form-control" name="username" id="first_name"
                                   value="${profile.username}">
                        </div>
                        <div class="form-group">
                            <label for="email"><h4>Email</h4></label>
                            <input type="email" class="form-control" name="email" id="email"
                                   value="${profile.email}" title="enter your email.">
                        </div>
                        <div class="form-group">
                            <label for="location"><h4>Country</h4></label>
                            <input class="form-control" id="location" value="${profile.country}"
                                   title="enter a country" name="country">
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <button class="btn btn-lg btn-success" name="submit" type="submit" onclick="clicked()">
                                    <i class="glyphicon glyphicon-ok-sign"></i>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>

                    <hr>

                </div><!--/tab-pane-->
            </div>
        </div>

    </form>

</div>
</body>
</html>